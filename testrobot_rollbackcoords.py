import socket
import time
import subprocess
import os
from time import sleep

class robotArm():
    def __init__(self):
        self.sock=socket.socket()
        self.sock.connect(("localhost",4446))
        self.testConnection()
        print("arm comm set up")

        self.movement_1 = (200,0,150), (105,120,175,300), (148,152,178,100), (148,152,195,50), (150,120,185,50), (256,119,80,200), None, (256,119,58,50), None, (256,119,50,20), (215,110,58,100), (200,0,150,300)
        self.movement_2 = (200,0,150), (215,110,58,300), (258,119,58,100), (258,119,80,25), (105,100,160,300), (145,152,195,300), (145,152,178,50), (105,120,175,500),(200,0,150)
        self.movement_3 = (200,0,150), (105,122,80,300), (145,153,80,100), (145,153,95,50), (256,119,80,200), None, (256,119,58,50), (215,110,58,100), (200,100,150,200),(200,0,150)
        self.movement_4 = (200,0,150), (215,110,58,300), (255,120,58,300), (255,120,80,25), (146,151,95,200), (146,151,80,50), (105,122,80,500), (200,0,150)
        self.movement_5 = (200,0,150), (215,110,80,400), (200,0,150,400)#this is a test movement
        self.movement_6 = (200,0,150), (105,120,175,500), (145,152,178,100), (145,152,195,50), (150,120,185,100), (255,120,80,200)

    def reset(self,speed=500):
        cmd = "{'func': 'reset', 'kwargs': {'speed': %s, 'wait': true}}" % speed
        msg = '{"message":"%s"}\n' % cmd
        self.sock.send(msg.encode())
        print(self.sock.recv(1024))

    def move_to_xyz(self,x,y,z,speed=500):
        cmd = "{'func': 'set_position', 'kwargs': {'x': %s, 'y': %s, 'z': %s, 'speed': %s, 'wait': true}}" % (x,y,z,speed)
        msg = '{"message":"%s"}\n' % cmd
        self.sock.send(msg.encode())
        print(self.sock.recv(1024))
        
    def testConnection(self):
        cmd = "{'func': 'get_device_info'}"
        msg = '{"message":"%s"}\n' % cmd
        self.sock.send(msg.encode())

    def moveSequence(self,args):     
        self.reset(500)
        for pos in args:
            print(pos)
            if pos==None:
                sleep(1)
            else:
                self.move_to_xyz(*pos)
        self.reset(500)
        
    def loadSample(self,sample):
        if sample in  {6, 7, 8, 9, 10}:
            self.moveSequence(self.movement_1)
        elif sample in {1, 2, 3, 4, 5}:
            self.moveSequence(self.movement_3)
        else:
            print("Choose either lower or upper sample")
            
    
    def unloadSample(self,sample):      
        if sample in  {6, 7, 8, 9, 10}:
            self.moveSequence(self.movement_2)
        elif sample in {1, 2, 3, 4, 5}:
            self.moveSequence(self.movement_4)
        else:
            print("Choose either lower or upper tier")
      
            
    def testMovement(self):
        self.moveSequence(self.movement_5)


if __name__ == "__main__":
    
    ra = robotArm()
    ra.unloadSample(6)
    time.sleep(5)
    ra.loadSample(6)
    #time.sleep(10)
    #ra.loadSample(1)
    #time.sleep(10)
    #ra.unloadSample(1)
    #ra.move_to_xyz(*(256,119,80,50))
    #ra.move_to_xyz(*(200,0,150,150))
