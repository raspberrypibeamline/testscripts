import subprocess

print('initiating connection with gda server')
cmd_str = 'ssh pi@localhost -p 2222 \'roboscan(%s)\'' % '1'
#cmd_str = 'ssh pi@localhost -p 2222 \'testscan()\
try:
    gda_server_status = subprocess.check_output(cmd_str, stderr=subprocess.STDOUT, shell=True)
    print("gda server status is: ", gda_server_status)
    if '!*scan failed*!' in gda_server_status.decode('utf-8'):
        print("the scan has failed")
	if 'non-zero exit status 124' in gda_server_status.decode('utf-8'):
	    print("failure caused by camera timeout")
except:
    raise
    print("exception encountered")

