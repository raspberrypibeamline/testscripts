import os
import subprocess
        
output_path = '/tmp/processed/model/'
create_dir_str = 'mkdir -p /tmp/processed/model'
dir_status = subprocess.check_output(create_dir_str, stderr=subprocess.STDOUT, shell=True)

cmd_str = 'python /home/pi/py_twitter/visualisation.py /home/pi/136/tomo_p1_visual_hulls_recon.h5 ' + output_path
try:
    #vis_status = os.system(cmd_str)
    vis_status = subprocess.check_output(cmd_str, stderr=subprocess.STDOUT, shell=True)

    print(vis_status)
except:
    print('some error occurred')
