#!/usr/bin/python
#--------------------------------------
#    ___  ___  _ ____          
#   / _ \/ _ \(_) __/__  __ __ 
#  / , _/ ___/ /\ \/ _ \/ // / 
# /_/|_/_/  /_/___/ .__/\_, /  
#                /_/   /___/   
#
#    Stepper Motor Test
#
# A simple script to control
# a stepper motor.
#
# Author : Matt Hawkins
# Date   : 28/09/2015
#
# http://www.raspberrypi-spy.co.uk/
#
#--------------------------------------

# Import required libraries
import sys
import time
import RPi.GPIO as GPIO

# Use BCM GPIO references
# instead of physical pin numbers
GPIO.setmode(GPIO.BCM)

class Stepper(object):
    def __init__(self,step_pins,step_scale_factor):
        # Define advanced sequence
        # as shown in manufacturers datasheet
        self.seq = [[1,0,0,1],
               [1,0,0,0],
               [1,1,0,0],
               [0,1,0,0],
               [0,1,1,0],
               [0,0,1,0],
               [0,0,1,1],
               [0,0,0,1]]
       
        self.step_pins = step_pins
        self.step_scale_factor = step_scale_factor

    def rotate(self,wait_time, deg_range):
        
        
        seq_length = len(self.seq) #this is an integer of 8 for our purposes

        step_dir = 1 # Set to 1 or 2 for clockwise # Set to -1 or -2 for anti-clockwise
        
        wait_time = wait_time/float(1000)
        #convert degrees to number of steps
        total_steps = float(4096)/float(360)*float(deg_range)*self.step_scale_factor

        if total_steps < 0:
            step_dir = -1 #acw ie. a negative input from gda
        else:
            step_dir = 1 #clockwise ie. positive

        seq_pos = 0
        step_counter=0
        
        # Start main loop
        while True:

          #print StepCounter,
          #print Seq[StepCounter]
          #print DegCounter

          for pin in range(0, 4):
            xpin = self.step_pins[pin]
            if self.seq[seq_pos][pin]!=0:
              #print " Enable GPIO %i" %(xpin)
              GPIO.output(xpin, True)
            else:
              GPIO.output(xpin, False)

          seq_pos += step_dir
          step_counter += step_dir
          if step_dir == -1:
              if (step_counter<=total_steps):
                  break
              else:
                  pass
          elif (step_counter>=total_steps):
                  break
          

          # If we reach the end of the sequence
          # start again
          if (seq_pos>=seq_length):
            seq_pos = 0
          if (seq_pos<0):
            seq_pos = seq_length+step_dir
            
            

          # Wait before moving on
          time.sleep(wait_time)
        GPIO.cleanup()
