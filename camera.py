#!/usr/bin/python

import sys
import os
from picamera import PiCamera
from os import system
from time import sleep
import RPi.GPIO as GPIO

# Use BCM GPIO references
# instead of physical pin numbers
GPIO.setmode(GPIO.BCM)
pin = 17

GPIO.setup(pin,GPIO.OUT)
GPIO.output(pin, GPIO.HIGH)

camera = PiCamera()
#camera.resolution = (1640,1232)
camera.resolution = (640, 480)
#camera.zoom = (0.35, 0.45, 0.25, 0.25)
camera.iso = 200
#shutter speed values should be between 
camera.shutter_speed = 6000
#camera.annotate_text = '@DiamondRPi'
print(sys.argv[0])
print(sys.argv[1])

try:
    camera.capture(sys.argv[1] + ".jpg", format="jpeg", quality = 100)
finally:
    camera.close()
    sleep(0.25)

print('scan saved at ' + sys.argv[1] + '.jpg')
GPIO.output(pin,GPIO.LOW)
