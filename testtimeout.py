import subprocess

try:
	cmd = 'timeout 15 {}'.format("python3 /home/pi/GDA-server-9.11.0.v20181107-1129-linux64/r03-config/device_scripts/camera.py /home/pi/testscripts/test1")
	print("working cmd is: " + cmd)
	x = subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
except subprocess.CalledProcessError as exc:
	if exc.returncode == 124:
        	print "Command timed out"
	else:
        	raise
