#!/usr/bin/python

import RPi.GPIO as GPIO
import time

# Use BCM GPIO references
# instead of physical pin numbers
GPIO.setmode(GPIO.BCM)

# Choose the test pins (BCM not physical)
#StepPins = range(1,27)
#StepPins = [1,5,6,7,8,9,10,11,12,13,14,15,16,17,19,20,21,22,23,24,25,26,27]
#StepPins = [6,13,19,26]
StepPins = [6,12,16,20,21]

# Set all pins as output
for pin in StepPins:
	GPIO.setup(pin,GPIO.OUT)
	GPIO.output(pin, GPIO.LOW) #set pins low

for pin in range(0, 27):
	xpin = StepPins[pin]
	print "pin " + str(xpin) + "turning on"
	GPIO.output(xpin, GPIO.HIGH) #set pin high
	time.sleep(4)
	print "pin " + str(xpin) + "turning off"
	GPIO.output(xpin, GPIO.LOW)

for pin in StepPins:
	GPIO.setup(pin,GPIO.OUT)
	GPIO.output(pin, GPIO.LOW) #set pins low