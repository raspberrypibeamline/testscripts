import socket
import time
import subprocess
import os
from time import sleep

class robotArm():
    def __init__(self):
        self.sock=socket.socket()
        self.sock.connect(("localhost",4446))
        self.testConnection()
        print("arm comm set up")

	HOME = (200,0,150)

	B_C_U = (105,122,175)
	A_C_U = (149,153,195)
	O_C_U = (149,153,178)
	X_C_U = (120,100,150)

	B_C_L = (105,122,80)
	A_C_L = (151,153,95)
	O_C_L = (151,153,80)

	A_T = (258,119,80)
	O_T = (258,119,58)
	B_T = (200,107,58)


        self.movement_1 = HOME, B_C_U+(300,), O_C_U+(100,), A_C_U+(50,), B_C_U+(100,), X_C_U+(100,), A_T+(200,), None, O_T+(25,), None, B_T+(50,), HOME
        self.movement_2 = HOME, B_T+(300,), O_T+(100,), A_T+(25,), X_C_U+(200,), B_C_U+(200,), A_C_U+(300,), O_C_U+(50,), B_C_U+(500,), HOME
        self.movement_3 = HOME, B_C_L+(300,), O_C_L+(100,), A_C_L+(50,), A_T+(200,), None, O_T+(25,), None, B_T+(50,), HOME
        self.movement_4 = HOME, B_T+(300,), O_T+(100,), A_T+(25,), B_C_L+(200,), A_C_L+(100,), O_C_L+(50,), B_C_L+(500,), HOME
        self.movement_5 = HOME, (215,110,80,400), (200,0,150,400)#this is a test movement
        self.movement_6 = HOME, (105,120,175,500), (145,152,178,100), (145,152,195,50), (150,120,185,100), (255,120,80,200)

    def reset(self,speed=500):
        cmd = "{'func': 'reset', 'kwargs': {'speed': %s, 'wait': true}}" % speed
        msg = '{"message":"%s"}\n' % cmd
        self.sock.send(msg.encode())
        print(self.sock.recv(1024))

    def move_to_xyz(self,x,y,z,speed=500):
        cmd = "{'func': 'set_position', 'kwargs': {'x': %s, 'y': %s, 'z': %s, 'speed': %s, 'wait': true}}" % (x,y,z,speed)
        msg = '{"message":"%s"}\n' % cmd
        self.sock.send(msg.encode())
        print(self.sock.recv(1024))
        
    def testConnection(self):
        cmd = "{'func': 'get_device_info'}"
        msg = '{"message":"%s"}\n' % cmd
        self.sock.send(msg.encode())

    def moveSequence(self,args):     
        self.reset(500)
        for pos in args:
            print(pos)
            if pos==None:
                sleep(1)
            else:
                self.move_to_xyz(*pos)
        self.reset(500)
        
    def loadSample(self,sample):
        if sample in  {6, 7, 8, 9, 10}:
            self.moveSequence(self.movement_1)
        elif sample in {1, 2, 3, 4, 5}:
            self.moveSequence(self.movement_3)
        else:
            print("Choose either lower or upper sample")
            
    
    def unloadSample(self,sample):      
        if sample in  {6, 7, 8, 9, 10}:
            self.moveSequence(self.movement_2)
        elif sample in {1, 2, 3, 4, 5}:
            self.moveSequence(self.movement_4)
        else:
            print("Choose either lower or upper tier")
      
            
    def testMovement(self):
        self.moveSequence(self.movement_5)


if __name__ == "__main__":
    
    ra = robotArm()
    ra.unloadSample(6)
    #time.sleep(5)
    ra.loadSample(6)
    #time.sleep(10)
    #ra.unloadSample(1)
    #time.sleep(10)
    #ra.loadSample(1)
    #ra.move_to_xyz(*(256,119,80,50))
    #ra.move_to_xyz(*(200,0,150,150))
